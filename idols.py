from dataclasses import dataclass
import logging
import requests
from typing import Any, Dict, List, Optional, Sequence, Tuple, Type, TypeVar, cast

from blaseball_mike import reference


_LOGGER = logging.getLogger('snaximum.idols')


# Aliases for SIBR structures
Player = Dict[str, Any]       # Reference player data object
StatSplit = Dict[str, Any]    # Reference stats split object
PlayerStats = Dict[str, Any]  # Reference player stats object; e.g. StatSplit['stat']


class StatsData:
    def __init__(self, season: int) -> None:
        self.season = season
        self._batters: Dict[str, StatSplit] = {}
        self._pitchers: Dict[str, StatSplit] = {}

    def refresh_batter_stats(self) -> None:
        _LOGGER.info("Getting batting statistics ...")
        data = reference.get_stats(
            type_='season', group='hitting',
            fields=('hits', 'home_runs', 'stolen_bases', 'appearances'),
            season=self.season
        )
        assert len(data) == 1
        for split in data[0]['splits']:
            self._batters[split['player']['id']] = split
        _LOGGER.info("OK. (%d batters)", len(self._batters))

    def refresh_pitcher_stats(self) -> None:
        _LOGGER.info("Getting pitching statistics ...")
        data = reference.get_stats(
            type_='season', group='pitching',
            fields=('games', 'shutouts', 'strikeouts', 'home_runs_allowed'),
            season=self.season
        )
        assert len(data) == 1
        for split in data[0]['splits']:
            self._pitchers[split['player']['id']] = split
        _LOGGER.info("OK. (%d pitchers)", len(self._pitchers))

    @property
    def batter_stats(self) -> Dict[str, StatSplit]:
        if not self._batters:
            self.refresh_batter_stats()
        return self._batters

    @property
    def pitcher_stats(self) -> Dict[str, StatSplit]:
        if not self._pitchers:
            self.refresh_pitcher_stats()
        return self._pitchers


class PlayerData:
    """
    Facilitate getting players by ID, via __getitem__.

    This implementation just gets *all* players at once, if it can.
    """
    def __init__(self) -> None:
        self._players: Optional[Dict[str, Player]] = None

    def _refresh(self) -> None:
        _LOGGER.info("Getting player data ...")
        self._players = {}
        rsp = requests.get('https://api.blaseball-reference.com/v2/players')
        assert rsp.status_code == 200
        players = rsp.json()
        _LOGGER.info("OK. (%d players)", len(players))
        for player in players:
            self._players[player['player_id']] = player

    def __getitem__(self, guid: str) -> Player:
        if self._players is None:
            self._refresh()
            assert self._players is not None
        return self._players[guid]


@dataclass
class PlayerAnalysis:
    player_id: str
    player_name: str
    team_emoji: str = ''
    status: str = ''
    modifier: int = 0
    gross_income: int = 0
    games_played: int = 0


@dataclass(eq=True, frozen=True)
class BatterSnackPayouts:
    """Coin payouts per-batter-event."""
    hit: int
    home_run: int
    stolen_base: int


class BatterAnalysis(PlayerAnalysis):
    # Relevant stats
    seed_hits: int = 0
    stolen_bases: int = 0
    home_runs: int = 0

    # Results
    hits_income: int = 0
    stolen_bases_income: int = 0
    home_runs_income: int = 0


@dataclass(eq=True, frozen=True)
class PitcherSnackPayouts:
    """Coin payouts per-pitcher-event."""
    strikeout: int
    shutout: int
    homerun: int


class PitcherAnalysis(PlayerAnalysis):
    # Relevant stats
    strikeouts: int = 0
    shutouts: int = 0
    home_runs_allowed: int = 0

    # Results
    strikeouts_income: int = 0
    shutouts_income: int = 0
    home_runs_income: int = 0


KNOWN_MODIFICATIONS = (
    'ALTERNATE',
    'CREDIT_TO_THE_TEAM',  # Modifier 5x
    'DOUBLE_PAYOUTS',      # Modifier 2x
    'ELSEWHERE',
    'FIRE_EATER',
    'FIRST_BORN',
    'FLICKERING',
    'FLIICKERRRIIING',
    'FLINCH',
    'FRIEND_OF_CROWS',
    'HARD_BOILED',
    'HAUNTED',
    'HONEY_ROASTED',
    'LIBERATED',
    'MARKED',
    'NON_IDOLIZED',
    'OVERPERFORMING',
    'OVERUNDER',
    'PERK',
    'RECEIVER',
    'REPEATING',
    'RETIRED',
    'RETURNED',
    'REVERBERATING',
    'SCATTERED',
    'SHELLED',
    'SIPHON',
    'SPICY',
    'SQUIDDISH',
    'STABLE',
    'SUPERALLERGIC',
    'SUPERYUMMY',
    'SWIM_BLADDER',
    'TRIPLE_THREAT',
    'UNDEROVER',
    'UNDERPERFORMING',
    'WANDERER',
    'WILD',
)


class Idols:
    def __init__(self, season: int) -> None:
        self.season = season
        self.data = StatsData(season)
        self.players = PlayerData()
        self.batter_analysis_cache: Dict[BatterSnackPayouts,
                                         List[BatterAnalysis]] = {}
        self.pitcher_analysis_cache: Dict[PitcherSnackPayouts,
                                          List[PitcherAnalysis]] = {}
        self._game_days = -1

    def refresh_batters(self) -> None:
        self.batter_analysis_cache = {}
        self.data.refresh_batter_stats()

    def refresh_pitchers(self) -> None:
        self.pitcher_analysis_cache = {}
        self.data.refresh_pitcher_stats()

    @property
    def game_days(self) -> int:
        if self._game_days == -1:
            self._game_days = max(
                split['stat']['appearances'] for split in self.data.batter_stats.values()
            )
        return self._game_days

    @classmethod
    def payout_modifier(cls, player: Player) -> int:
        """For a given player, return their idol payout modifier."""
        modifications = player.get('modifications', [])

        if 'CREDIT_TO_THE_TEAM' in modifications:
            return 5
        if 'DOUBLE_PAYOUTS' in modifications:
            return 2

        return 1

    @classmethod
    def scrutinize_modifications(cls, player: Player) -> None:
        for mod in player.get('modifications', []):
            if mod not in KNOWN_MODIFICATIONS:
                _LOGGER.warning("Unrecognized player mod '%s'", mod)

    @classmethod
    def scrutinize_whereabouts(cls, player: Player) -> None:
        if player['current_location'] not in ('main_roster', 'shadow_fk',
                                              'shadow_known', None):
            _LOGGER.warning("%s [%s]: Unknown player location '%s'",
                            player['player_name'],
                            player['player_id'],
                            player['current_location'])

        if player['current_state'] not in ('deceased', 'retired', 'deprecated',
                                           'active', None):
            _LOGGER.warning("%s [%s]: Unknown player state '%s'",
                            player['player_name'],
                            player['player_id'],
                            player['current_state'])

        if (player['current_location'] == None and
            player['current_state'] not in ('deceased', 'retired', 'deprecated')):
            _LOGGER.warning(
                "%s [%s]: Player location was 'None', "
                "but Player was not known to be deceased/retired/deprecated",
                player['player_name'],
                player['player_id']
            )

        if (player['current_state'] == None and
            player['current_location'] not in ('main_roster', 'shadow_known', 'shadow_fk')):
            _LOGGER.warning(
                "%s [%s]: Player state was 'None', "
                "but Player was not in the Shadows or on the Main Roster.",
                                player['player_name'],
                player['player_id']
            )

    @classmethod
    def disqualifying_statuses(cls, player: Player) -> str:
        status = ''

        if player['deceased'] or player['current_state'] == 'deceased':
            status += '💀'
        if player['current_location'] in ('shadow_known', 'shadow_fk'):
            status += '🔃'
        if player['current_state'] == 'retired':
            status += '🛏💤'
        if player['current_state'] == 'deprecated':
            if player['player_id'] == 'bc4187fa-459a-4c06-bbf2-4e0e013d27ce':
                status += '🐶🌊'
            else:
                status += '🌌'
        if 'ELSEWHERE' in player['modifications']:
            status += '🌊'
        if 'SHELLED' in player['modifications']:
            status += '🥜'

        return status

    T = TypeVar('T', bound=PlayerAnalysis)
    def _analysis_common(self,
                         player_guid: str,
                         analysis_class: Type[T],
                         split: StatSplit) -> T:

        player = self.players[player_guid]
        self.scrutinize_modifications(player)
        self.scrutinize_whereabouts(player)

        res = analysis_class(player['player_id'], player['player_name'])

        res.modifier = self.payout_modifier(player)
        res.status = self.disqualifying_statuses(player)

        if split['team']['team_emoji'].startswith('0x'):
            res.team_emoji = chr(int(split['team']['team_emoji'], 16))
        else:
            res.team_emoji = split['team']['team_emoji']

        return res

    def batter_analysis(self, player_guid: str,
                        bspayouts: BatterSnackPayouts) -> BatterAnalysis:
        """
        For a given batter, calculate how much money you would have received
        given a specific assortment of batter idol related snacks.
        """
        split = self.data.batter_stats[player_guid]
        stats = split['stat']
        res = self._analysis_common(player_guid, BatterAnalysis, split)

        res.seed_hits = stats['hits'] - stats['home_runs']
        res.home_runs = stats['home_runs']
        res.stolen_bases = stats['stolen_bases']

        res.games_played = stats['appearances']

        res.hits_income = res.modifier * res.seed_hits * bspayouts.hit
        res.home_runs_income = res.modifier * res.home_runs * bspayouts.home_run
        res.stolen_bases_income = res.modifier * res.stolen_bases * bspayouts.stolen_base
        res.gross_income = res.hits_income + res.home_runs_income + res.stolen_bases_income

        return res

    def pitcher_analysis(self, player_guid: str,
                         pspayouts: PitcherSnackPayouts) -> PitcherAnalysis:
        """
        For a given pitcher, calculate how much money you would have
        received given a specific assortment of pitcher idol related
        snacks.
        """
        split = self.data.pitcher_stats[player_guid]
        stats = split['stat']
        res = self._analysis_common(player_guid, PitcherAnalysis, split)

        res.strikeouts = stats['strikeouts']
        res.shutouts = stats['shutouts']
        res.home_runs_allowed = stats['home_runs_allowed']
        res.games_played = stats['games']

        res.strikeouts_income = res.modifier * res.strikeouts * pspayouts.strikeout
        res.shutouts_income = res.modifier * res.shutouts * pspayouts.shutout
        res.home_runs_income = res.modifier * res.home_runs_allowed * pspayouts.homerun
        res.gross_income = res.strikeouts_income + res.shutouts_income + res.home_runs_income

        return res

    def batters_analysis(self, bspayouts: BatterSnackPayouts,
                         limit: int = 10) -> List[BatterAnalysis]:
        """
        Return a list of all batters, their stats, and their profitability
        assessments; sorted by their profitability based on the specified
        snack assortment.
        """
        if bspayouts in self.batter_analysis_cache:
            return self.batter_analysis_cache[bspayouts][0:limit]

        batters = []
        for batter_guid in self.data.batter_stats:
            analysis = self.batter_analysis(batter_guid, bspayouts)
            batters.append(analysis)

        batters = sorted(batters, key=lambda ba: ba.gross_income, reverse=True)
        self.batter_analysis_cache[bspayouts] = batters
        return batters[0:limit]

    def pitchers_analysis(self, pspayouts: PitcherSnackPayouts,
                         limit: int = 10) -> List[PitcherAnalysis]:
        """
        Return a list of all pitchers, their stats, and their profitability
        assessments; sorted by their profitability based on the specified
        snack assortment.
        """
        if pspayouts in self.pitcher_analysis_cache:
            return self.pitcher_analysis_cache[pspayouts][0:limit]

        pitchers = []
        for pitcher_guid in self.data.pitcher_stats:
            analysis = self.pitcher_analysis(pitcher_guid, pspayouts)
            pitchers.append(analysis)

        pitchers = sorted(pitchers, key=lambda pa: pa.gross_income, reverse=True)
        self.pitcher_analysis_cache[pspayouts] = pitchers
        return pitchers[0:limit]

    def print_analysis(self, batters: Sequence[BatterAnalysis]) -> None:
        """
        Print to the terminal the top 10 batters, based on profitability given
        the specified snack assortment.
        """
        for analysis in batters:
            print("{:6d} ({:3d}H-HR, {:2d}HR, {:2d}SB) {:7.2f}/game {:s} {:21s}{:s}".format(
                analysis.gross_income,
                analysis.seed_hits,
                analysis.home_runs,
                analysis.stolen_bases,
                analysis.gross_income / analysis.games_played,
                analysis.team_emoji,
                analysis.player_name,
                analysis.status
            ))

    def print_pitching_analysis(self, pitchers: Sequence[PitcherAnalysis]) -> None:
        """
        Print to the terminal the top 10 pitchers, based on profitability given
        the specified snack assortment.
        """
        for analysis in pitchers:
            print("{:6d} (Games: {:2d}, SO: {:3d}, SHO: {:2d}; HR: {:2d}) {:8.2f}/game {:s} {:21s}{:s}".format(
                analysis.gross_income,
                analysis.games_played,
                analysis.strikeouts,
                analysis.shutouts,
                analysis.home_runs_allowed,
                analysis.gross_income / analysis.games_played,
                analysis.team_emoji,
                analysis.player_name,
                analysis.status
            ))
