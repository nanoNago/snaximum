"""
Slightly less hasty POC for optimizing snack purchasing.

(THESE DOCS VERY OUT OF DATE, LOL)

Two models/strategies are offered:

1. Optimizing your income-per-day ("LONG_TERM"), or
2. Optimizing your ROI as measured at the end of the season ("SEASON").

LONG_TERM relevant values:
 - Δgross: increase in hypothetical income since day0 of this season
 - Δipd: increase in Income Per Day (IPD).
 - ratio: Δipd / cost-of-upgrade.

SEASON relevant values:
 - gross(S): income through end-of-season as a result of this purchase
 - profit(S): gross(S) minus the cost. Negative won't pay for itself this season.
 - ROI(S): Seasonal ROI (profit(S) / cost); Negative won't pay for itself this season.


Caveats:

Data problems:

1. Data comes mostly from Chronicler and Blaseball-Reference. Sometimes these
   sources are gently out of date.

2. Number of players washed away in floods is still manually entered.

3. Startup cost is somewhat high. Not every last player needs to be queried at
   startup.

4. Player lookup (for checking for modifications when computing payouts)
   could likely be done on-demand after an initial bulk-query. Right now,
   if a player is missing in fetch data we assume they don't have any
   interesting modifications. This might not be true.


Model problems:

1. Payout data is normalized per-diem, but clumsily. This undervalues batters
   introduced late in a season.

2. Deceased, Shelled, Elsewhere, etc batters do not have ther per-diem payouts
   set to 0.

3. Payout data is not capable of recognizing abrupt shifts in average; e.g. if a
   batter starts performing massively better or worse. The same holds true for
   Black Hole or Flooded Runners calculations. It is based on the entire season
   only.

4. Early season recommendations are going to be very wrong until the data
   settles out; previous season's data is not consulted.

5. Snacks are optimized per-snack instead of per-snaxfolio. This means that
   if the next best upgrade is something expensive (like a wetzel), the
   algorithm will prefer to "wait" for it, even if you can afford other
   upgrades in the meantime.

   A limited stop-gap against this is the `impatient=True` setting, which
   will attempt to spend ALL of the coins, even with suboptimal purchases.

   However, the model is still per-snack and does not measure a snaxfolio-wide
   ROI, which *may* offer slightly different choices that are better in the
   extremely near-term.

6. Idol switching costs are not modeled. They are presumed negligible so far.

7. Popcorn, Stale Popcorn, Burgers and Chips are not modeled at present.

8. Snake Oil recommendations are based on your estimated "betting consistency",
   i.e. how many games per day you actually bet on. It's only a rough estimate;
   maybe all the lucrative betting games are while you're asleep? Your waking
   and sleeping hours are not modeled.


# Initializing

>>> from snax import Snaximum
>>> snax = Snaximum()
Opening upgrades.json ...OK
Getting player data ...OK
Getting batting statistics ...OK
Getting black hole data ...OK (20)
Getting bet data ...OK
Calculating optimal betting threshold ...0.53
  (Override via `betting_threshold` property if desired.)
Assuming flooded runners count this season is 180
  (Adjust via `flooded_runners` property as needed.)
Assuming your betting consistency is 0.58
  (Adjust via `betting_consistency` as desired. Set to 0.0 for full-passive.)
>>>


# Updating flooded runners counts

>>> snax.runners_flooded = 180


# Setting your betting preferences:
# consistency of 0.00 is pure passive, 1.00 is perfect robot.
# 14/24 should be a reasonable target for hardcore players.
# 6/24 is probably good if you bet only sometimes.

>>> snax.betting_threshold = 0.51
>>> snax.betting_consistency = (14/24)


# Viewing leaderboards, assuming maximum snacks owned.  Results show the
# total amount of money that could have been earned by that idol for
# existing play this season.

>>> snax.lucrative_batters()
277000 (62H, 10HR, 48SB) Goodwin Morin
273000 (62H, 15HR, 40SB) Don Mitchell
268500 (71H, 18HR, 30SB) Aldon Cashmoney
249000 (76H, 21HR, 17SB) Valentine Games
240000 (90H, 15HR, 15SB) Dudley Mueller
239500 (67H, 13HR, 29SB) Comfort Septemberish
208000 (82H, 16HR,  7SB) Jaxon Buckley
202000 (38H,  1HR, 47SB) Richardson Games
199500 (57H, 15HR, 18SB) Conner Haley
199000 (82H, 13HR,  8SB) Baby Doyle


# The same command, but with a specific assortment of snacks:

>>> snaxfolio = {'dogs': 26, 'seeds': 95, 'pickles': 56, 'slushies': 85,
                 'wetzels': 24, 'snoil': 64}
>>> snax.lucrative_batters(snaxfolio)
180664 (62H, 10HR, 48SB) Goodwin Morin
172199 (62H, 15HR, 40SB) Don Mitchell
171097 (71H, 18HR, 30SB) Aldon Cashmoney
169530 (90H, 15HR, 15SB) Dudley Mueller
159172 (76H, 21HR, 17SB) Valentine Games
158509 (67H, 13HR, 29SB) Comfort Septemberish
145509 (82H, 16HR,  7SB) Jaxon Buckley
144109 (82H, 13HR,  8SB) Baby Doyle
135470 (40H,  7HR,  2SB) York Silk
135396 (38H,  1HR, 47SB) Richardson Games



# Getting a proposed snack upgrade schedule. If a snack purchase makes a
# new idol more profitable in retrospect, it will print out a
# recommendation to switch. It will always print this line before the
# first pickle/dog/seed item. It doesn't print out which idol it assumes
# you have, though.

# The "dx" is the increase in the "profit you would have had ..." figure.
# ratio is just dx/cost, showing the relative gain for your troubles.

# This is an empty upgrade schedule, cut off after ten items:
# (The value returned is a new snaxfolio with updated counts.)

>>> snax.propose_upgrades(cash=50000)
01: Buy snoil    for    0 (dx: 1283; ratio: inf)
--- Switch idol to Goodwin Morin
02: Buy pickles  for   10 (dx: 2400; ratio: 240.000)
03: Buy slushies for   10 (dx: 1800; ratio: 180.000)
04: Buy slushies for   10 (dx:  900; ratio: 90.000)
05: Buy pickles  for   20 (dx: 1440; ratio: 72.000)
06: Buy snoil    for   20 (dx: 1292; ratio: 64.600)
07: Buy slushies for   15 (dx:  900; ratio: 60.000)
08: Buy slushies for   20 (dx:  900; ratio: 45.000)
09: Buy pickles  for   35 (dx: 1440; ratio: 41.143)
10: Buy snoil    for   40 (dx: 1283; ratio: 32.075)
...
{'dogs': 9, 'seeds': 15, 'pickles': 26, 'wetzels': 10,
 'slushies': 41, 'popcorn': 0, 'stalecorn': 0, 'snoil': 22,
 'chips': 0, 'burgers': 0}


# Proposed snack schedule upgrade for a specific array of snacks.
# Note that the suggestion doesn't know who you are idoling, so the first snack
# purchase that depends on an idol will note which idol provides that value.
# (The value returned is a new snaxfolio with updated counts.)

>>> snaxfolio = {'dogs': 26, 'seeds': 95, 'pickles': 51,
                 'slushies': 65, 'wetzels': 15}
>>> snax.propose_upgrades(cash=50000, snaxfolio=snaxfolio)
01: Buy wetzels  for 7475 (dx: 2945; ratio: 0.394)
02: Buy slushies for 2390 (dx:  900; ratio: 0.377)
--- Switch idol to Goodwin Morin
03: Buy pickles  for 3895 (dx: 1440; ratio: 0.370)
04: Buy slushies for 2435 (dx:  900; ratio: 0.370)
05: Buy wetzels  for 7845 (dx: 2850; ratio: 0.363)
06: Buy slushies for 2480 (dx:  900; ratio: 0.363)
07: Buy pickles  for 4000 (dx: 1440; ratio: 0.360)
08: Buy wetzels  for 8220 (dx: 2945; ratio: 0.358)
09: Buy slushies for 2525 (dx:  900; ratio: 0.356)
10: Buy pickles  for 4105 (dx: 1440; ratio: 0.351)
11: Buy slushies for 2575 (dx:  900; ratio: 0.350)
{'dogs': 26, 'seeds': 95, 'pickles': 59, 'slushies': 90, 'wetzels': 27,
 'snoil': 64, 'popcorn': 0, 'stalecorn': 0, 'chips': 0, 'burgers': 0}


# Upgrades won't overflow beyond their maximum:

>>> snax.propose_upgrades(cash=50000, snaxfolio=snax.mksnax(maximum=True))
--- No further upgrades available ---
{'dogs': 99, 'seeds': 99, 'pickles': 99, 'wetzels': 99, 'slushies': 99,
 'popcorn': 99, 'stalecorn': 99, 'snoil': 99, 'chips': 99, 'burgers': 99}


Hint: try setting your betting consistency to 0.00 for your
last suggestion of the night before you sleep.

"""

from enum import Enum
import logging
import math
import json
from typing import Any, Dict, Iterable, List, Optional, cast

from blaseball_mike import chronicler, database
from blaseball_mike.tables import Weather

from bets import Bets, Maxbet
from idols import Idols, BatterSnackPayouts, PitcherSnackPayouts


_LOGGER  = logging.getLogger('snaxfolio')


# Aliases for Blaseball structures
ItemTier = Dict[str, int]  # contains 'price' and 'amount' values
UpgradeData = Dict[str, List[ItemTier]]  # e.g. blackHoleTiers -> List[ItemTier]

# Mapping of snacks to number owned.
Snaxfolio = Dict[str, int]
# Upgrade analysis dict (cost, dx, ratio, etc.)
PurchaseAnalysis = Dict[str, Any]


class Strategy(Enum):
    LONG_TERM = 1
    SEASONAL = 2


class Snaximum:
    def __init__(self) -> None:
        # Manually updated statistics:
        self.flooded_runners = 292

        # State
        self._season = 0
        self._day = 0
        self._black_holes = -1
        self._sun2_events = -1
        self._incinerations = -1
        self._upgrades: UpgradeData = {}

        # Tunables:
        self._default_bet_threshold = 0.0
        self.default_bet_consistency = (14/24)

        # Specialized Sims:
        self.bets: Bets
        self.idols: Idols

        # Internal/etc
        self.upgrade_map = {
            # Listed in order as they appear in the Concessions menu.
            'snoil': 'maxBetTiers',
            # 'popcorn': 'teamWinCoinTiers',
            # 'stalecorn': 'teamLossCoinTiers',
            # 'breakfast': 'timeOffTiers',
            # 'taffy': 'teamShamingTiers',
            # 'lemonade': 'teamShamedTiers',
            'chips': 'idolStrikeoutsTiers',
            'burgers': 'idolShutoutsTiers',
            'meatball': 'idolHomerAllowedTiers',
            'dogs': 'idolHomersTiers',
            'seeds': 'idolHitsTiers',
            'pickles': 'idolStealTiers',
            'slushies': 'floodClearTiers',
            'sundae': 'incinerationTiers',
            'wetzels': 'blackHoleTiers',
            'donuts': 'sunTwoTiers',
        }

        # Whee
        self.refresh()

    @property
    def season(self) -> int:
        if not self._season:
            self._season = database.get_simulation_data()['season'] + 1
        return self._season

    @property
    def day(self) -> int:
        if not self._day:
            self._day = database.get_simulation_data()['day'] + 1
        return self._day

    def _query_weather_outcomes(self, what: str, phrase: str,
                                weather: Weather) -> int:
        # Thanks for your help with this one, Walnut!
        _LOGGER.info("Getting %s data ...", what)
        data = chronicler.get_games(
                season=self.season,
                weather=weather.value,
                outcomes=True
        )
        result = sum(
            (phrase in o) for game in data for o in game['data']['outcomes']
        )
        _LOGGER.info(" OK (%d)", result)
        return result

    @property
    def black_holes(self) -> int:
        if self._black_holes == -1:
            self._black_holes = self._query_weather_outcomes(
                what='black holes',
                phrase='The Black Hole swallowed a Win',
                weather=Weather.BLACK_HOLE
            )
        return self._black_holes

    @property
    def sun2_events(self) -> int:
        if self._sun2_events == -1:
            self._sun2_events = self._query_weather_outcomes(
                what='sun2-granted wins',
                phrase='Sun 2 set a Win',
                weather=Weather.SUNNY
            )
        return self._sun2_events

    @property
    def incinerations(self) -> int:
        if self._incinerations == -1:
            self._incinerations = self._query_weather_outcomes(
                what='incinerations',
                phrase='Rogue Umpire incinerated',
                weather=Weather.SOLAR_ECLIPSE
            )
        return self._incinerations

    @property
    def upgrades(self) -> UpgradeData:
        if not self._upgrades:
            _LOGGER.info("Opening upgrades.json ...")
            with open("upgrades.json", "r") as infile:
                self._upgrades = json.load(infile)
            _LOGGER.info("OK")
        return self._upgrades

    @property
    def default_bet_threshold(self) -> float:
        if not self._default_bet_threshold:
            _LOGGER.info("Calculating optimal betting threshold ...")
            thresh = Maxbet.tune_threshold(self.bets.games)
            self._default_bet_threshold = thresh
            _LOGGER.info("Default bet threshold set to %3.2f", thresh)
        return self._default_bet_threshold

    @default_bet_threshold.setter
    def default_bet_threshold(self, value: float) -> None:
        self._default_bet_threshold = value

    def refresh(self) -> None:
        self.idols = Idols(self.season)
        self.bets = Bets.make(self.season)

    @property
    def days_remaining(self) -> int:
        return min(99 - self.day, 0)

    def _get_tiers(self, item_name: str) -> List[Dict[str, int]]:
        """Get upgrade tiers based on our informal name (snoil, dogs, etc.)"""
        return self.upgrades[self.upgrade_map[item_name]]

    def get_payout(self, item_name: str, owned: int) -> int:
        """
        How much does this many {item} pay out when it triggers?
        For snoil, how large is your betting cap?
        """
        if not owned:
            return 0
        return self._get_tiers(item_name)[owned - 1]['amount']

    def get_batter_snack_payouts(self, snax: Snaxfolio) -> BatterSnackPayouts:
        return BatterSnackPayouts(
            hit = self.get_payout('seeds', snax['seeds']),
            home_run = self.get_payout('dogs', snax['dogs']),
            stolen_base = self.get_payout('pickles', snax['pickles'])
        )

    def get_pitcher_snack_payouts(self, snax: Snaxfolio) -> PitcherSnackPayouts:
        return PitcherSnackPayouts(
            strikeout = self.get_payout('chips', snax['chips']),
            shutout = self.get_payout('burgers', snax['burgers']),
            homerun = self.get_payout('meatball', snax['meatball'])
        )

    def get_cost(self, item_name: str, owned: int) -> int:
        """
        How much does it cost to upgrade {item} if you have {owned} amount?
        """
        tiers = self._get_tiers(item_name)
        return tiers[owned]['price']

    def upgrades_left(self, item_name: str, owned: int) -> int:
        """
        How many times can you upgrade this item if you have {owned} amount?
        """
        tiers = self._get_tiers(item_name)
        return len(tiers) - owned

    def mksnax(self, snaxfolio: Optional[Snaxfolio] = None,
               maximum: bool = False) -> Snaxfolio:
        """
        Given a snack portfolio (Snaxfolio:tm:), return a fully-instantiated
        copy. if Maximum is true (SNAXIMUM:tm:), set all snacks to the maximum
        amount possible to own, however many that may be.
        """
        snax: Dict[str, int]
        if not snaxfolio:
            snax = {}
        else:
            snax = snaxfolio.copy()

        for item_name, tier_name in self.upgrade_map.items():
            tier_name = self.upgrade_map[item_name]
            snax.setdefault(
                item_name, len(self.upgrades[tier_name]) if maximum else 0
            )
        return snax

    def what_if(self, snaxfolio: Snaxfolio,
                which: str) -> Optional[PurchaseAnalysis]:
        """
        Compute profitability analysis on what would have happened if you had
        one more of the named snack (which) at the beginning of this season.

        Returns a dict with these fields:
        - 'which': which snack was purchased; as passed in.
        - 'dx': How much extra profit would have been made this season
        - 'cost': How much the snack costs you right now.
        - 'ratio': dx/cost. Higher is better.
        - 'snaxfolio': A new Snaxfolio with one item incremented.
        - 'idol': Which idol this assessment is based on. It may be None,
                  indicating it doesn't factor into the assessment.

        Note: the 'dx' and therefore also the 'ratio' will be penalized for
              snake-oil analyses based on the specified betting_consistency.

        May return None; when the specified snack cannot be upgraded.
        """
        snaxfolio = self.mksnax(snaxfolio)

        if self.upgrades_left(which, snaxfolio[which]) <= 0:
            return None

        # Idol-related items will set this as needed.
        idol = None

        if which == 'wetzels':
            current_gross = self.black_holes * self.get_payout('wetzels', snaxfolio[which])
            new_gross = self.black_holes * self.get_payout('wetzels', snaxfolio[which] + 1)

            # Wetzels and Bets both use the chronicler data - so use bets.days.
            days_seen = self.bets.days

        elif which == 'donuts':
            current_gross = self.sun2_events * self.get_payout('donuts', snaxfolio[which])
            new_gross = self.sun2_events * self.get_payout('donuts', snaxfolio[which] + 1)

            # Same as above; weather uses Chronicler data, same as bets.
            days_seen = self.bets.days

        elif which == 'sundae':
            current_gross = self.incinerations * self.get_payout('sundae', snaxfolio[which])
            new_gross = self.incinerations * self.get_payout('sundae', snaxfolio[which] + 1)

            # Same as above; weather uses Chronicler data, same as bets.
            days_seen = self.bets.days

        elif which == 'slushies':
            current_gross = self.flooded_runners * self.get_payout('slushies', snaxfolio[which])
            new_gross = self.flooded_runners * self.get_payout('slushies', snaxfolio[which] + 1)

            # Best guess.
            days_seen = self.day

        elif which == 'snoil':
            current_gross = self.bets.profit(
                Maxbet(bet=self.get_payout('snoil', snaxfolio[which]),
                       threshold=self.default_bet_threshold),
                efficiency=self.default_bet_consistency
            )
            new_gross = self.bets.profit(
                Maxbet(bet=self.get_payout('snoil', snaxfolio[which] + 1),
                       threshold=self.default_bet_threshold),
                efficiency=self.default_bet_consistency
            )

            days_seen = self.bets.days

        elif which in ('dogs', 'seeds', 'pickles'):

            # Current analysis
            reference = self.idols.batters_analysis(
                self.get_batter_snack_payouts(snaxfolio))[0]

            # Hypothetical Analysis
            snaxfolio[which] += 1
            best = self.idols.batters_analysis(
                self.get_batter_snack_payouts(snaxfolio))[0]
            snaxfolio[which] -= 1  # Put it back, we'll get it below.

            current_gross = reference.gross_income
            new_gross = best.gross_income

            days_seen = self.idols.game_days
            idol = {'player_id': best.player_id,
                    'player_name': best.player_name}

        elif which in ('chips', 'burgers', 'meatball'):
            reference = self.idols.pitchers_analysis(
                self.get_pitcher_snack_payouts(snaxfolio))[0]

            snaxfolio[which] += 1
            best = self.idols.pitchers_analysis(
                self.get_pitcher_snack_payouts(snaxfolio))[0]
            snaxfolio[which] -= 1

            current_gross = reference.gross_income
            new_gross = best.gross_income

            days_seen = self.idols.game_days
            idol = {'player_id': best.player_id,
                    'player_name': best.player_name}

        # Theoretical increase in gross income from day0 until now:
        delta_gross = new_gross - current_gross

        # Delta Income-Per-Day: change in estimated income per game-day.
        dipd = delta_gross / days_seen

        # How much does it cost to upgrade this snack?
        cost = self.get_cost(which, snaxfolio[which])

        # Increase in income-per-day, per-coin spent. This is a long-term strategy.
        ratio = (dipd / cost) if cost else math.inf

        # The seasonal gross income is the delta income-per-day * days_left
        seasonal_gross_income = (99 - days_seen) * dipd

        # Seasonal profit is simply the seasonal_gross_income - cost.
        seasonal_profit = seasonal_gross_income - cost

        # Seasonal ROI is (seasonal_profit / cost).
        seasonal_roi = (seasonal_profit / cost) if cost else math.inf

        # Increment the snaxfolio and return
        snaxfolio[which] += 1

        return {
            'which': which,  # Which item is this analysis for?
            'cost': cost,
            'idol': idol,    # Under which idol are these figures calculated?
            'snaxfolio': snaxfolio,  # (Adjusted for new assortment.)

            'Δgross': delta_gross,
            'Δipd': dipd,
            'ratio': ratio,         # Delta-Income-Per-Day / Cost

            'sgi': seasonal_gross_income,
            'sprofit': seasonal_profit,
            'sroi': seasonal_roi,
        }

    def calc_upgrade_costs(self, snaxfolio: Optional[Snaxfolio],
                           strategy: Strategy = Strategy.LONG_TERM
                           ) -> List[PurchaseAnalysis]:
        """
        For a given Snaxfolio:tm:, return a sorted list of the best choices to
        make at this point in time.

        Items set to -1 are presumed to be prohibited for this calculation.

        Impossible upgrades will not be returned; if no upgrades are possible,
        the list will be empty.
        """
        snaxfolio = self.mksnax(snaxfolio)
        choices = [self.what_if(snaxfolio, item)
                   for item in self.upgrade_map
                   if snaxfolio.get(item) != -1]

        if strategy == Strategy.LONG_TERM:
            sort_key = 'ratio'
        else:
            sort_key = 'sroi'

        return sorted(
            # mypy can't infer that filter(None, ...) sheds Optional[T]
            cast(Iterable[PurchaseAnalysis], filter(None, choices)),
            key=lambda x: cast(float, x[sort_key]),
            reverse=True
        )

    def propose_upgrade(self, snaxfolio: Optional[Snaxfolio],
                        strategy: Strategy = Strategy.LONG_TERM) -> None:
        """
        For a given Snaxfolio, print to the terminal all of the upgrades
        available to you, sorted by their assumed profitability.

        This is a single-purchase printout.
        """
        snaxfolio = self.mksnax(snaxfolio)
        proposals = self.calc_upgrade_costs(snaxfolio, strategy)
        if not proposals:
            print("--- No further upgrades available ---")
            return

        for proposal in proposals:
            print("{:8s}: cost {:4d}; Δgross: {:4d}; Δipd: {:5.2f}; ratio: {:4.2f}; ".format(
                proposal['which'],
                proposal['cost'],
                proposal['Δgross'],
                proposal['Δipd'],
                proposal['ratio']
            ), end='')
            print("gross(S): {:7.2f}; profit(S): {:7.2f}; ROI(S): {:5.2f}".format(
                proposal['sgi'],
                proposal['sprofit'],
                proposal['sroi'],
            ))

    def do_propose_upgrades(self, coins: int = 250,
                            snaxfolio: Optional[Snaxfolio] = None,
                            strategy: Strategy = Strategy.LONG_TERM,
                            impatient: bool = False
                            ) -> List[Optional[PurchaseAnalysis]]:
        snaxfolio = self.mksnax(snaxfolio)
        schedule: List[Optional[PurchaseAnalysis]] = []
        spent = 0

        while True:
            proposals = self.calc_upgrade_costs(snaxfolio, strategy)
            if not proposals:
                schedule.append(None)
                # --- No further upgrades available ---
                break

            if not impatient:
                proposal = proposals[0]
            else:
                for proposal in proposals:
                    if spent + proposal['cost'] <= coins:
                        # We can afford this one!
                        break

            if proposal != proposals[0]:
                # This choice is not necessarily optimal
                proposal['impatient'] = True

            spent += proposal['cost']
            if spent > coins:
                break

            schedule.append(proposal)
            snaxfolio = proposal['snaxfolio']
        return schedule

    def propose_upgrades(self, coins: int = 250,
                         snaxfolio: Optional[Snaxfolio] = None,
                         strategy: Strategy = Strategy.LONG_TERM,
                         impatient: bool = False) -> Snaxfolio:
        """
        Print to screen an accounting of which upgrades to buy and in which
        order, given a finite budget specified by `coins`.

        If a Snaxfolio is not provided, it assumes a completely empty one.

        If impatient is True, recommend sub-optimal upgrades if they are all you
        can afford. These items will be marked with a trailing '**'.
        """
        snaxfolio = self.mksnax(snaxfolio)
        schedule = self.do_propose_upgrades(coins, snaxfolio, strategy, impatient)
        idol = None
        i = 0
        summary: Dict[str, int] = {}

        for proposal in schedule:
            if proposal is None:
                print("--- No further upgrades available ---")
                break

            i += 1
            if proposal['idol'] is not None:
                if idol != proposal['idol']:
                    print("--- Switch idol to {}".format(proposal['idol']['player_name']))
                    idol = proposal['idol']

            snaxfolio = proposal['snaxfolio']

            preamble = f"{i:02d}:"
            suggestion = "Buy {which:8s} for {cost:4d}".format(**proposal)
            if strategy == Strategy.LONG_TERM:
                stats = "(ratio: {ratio:4.2f}; Δipd: {Δipd:5.2f})".format(**proposal)
            else:
                stats = "(ROI(S): {sroi:5.2f}; profit(S): {sprofit:7.2f})".format(**proposal)
            marker = ' **' if proposal.get('impatient') else ''
            print(preamble, suggestion, stats, marker)
            summary.setdefault(proposal['which'], 0)
            summary[proposal['which']] += 1

        print("Total spent: {}".format(
            sum(p['cost'] for p in schedule if p is not None)))
        print("Recommended purchase summary: ", str(summary))

        assert snaxfolio is not None
        return snaxfolio

    def lucrative_batters(self, snaxfolio: Optional[Snaxfolio] = None,
                          limit: int = 10) -> None:
        """
        Print to the terminal the top 10 batters, based on profitability given
        the specified snack assortment.
        """
        snaxfolio = self.mksnax(snaxfolio, maximum=True)
        batsnax = self.get_batter_snack_payouts(snaxfolio)
        batters = self.idols.batters_analysis(batsnax, limit)
        self.idols.print_analysis(batters)

    def lucrative_pitchers(self, snaxfolio: Optional[Snaxfolio] = None,
                           limit: int = 10) -> None:
        """
        Print to the terminal the top 10 batters, based on profitability given
        the specified snack assortment.
        """
        snaxfolio = self.mksnax(snaxfolio, maximum=True)
        payouts = self.get_pitcher_snack_payouts(snaxfolio)
        pitchers = self.idols.pitchers_analysis(payouts, limit)
        self.idols.print_pitching_analysis(pitchers)
