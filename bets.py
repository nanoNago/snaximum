from dataclasses import dataclass
from enum import Enum
import logging
import math
from typing import Any, Dict, List, Optional, Tuple

from blaseball_mike import chronicler


# Chronicler game data object
Game = Dict[str, Any]
# (num_games, payout)
PayoutData = Tuple[int, int]


_LOGGER = logging.getLogger('snaximum.bets')


class BetData:
    def __init__(self, season: int) -> None:
        self.season = season
        self._games: Optional[List[Game]] = None

    @property
    def games(self) -> List[Game]:
        """A list of completed games as returned from Chronicler."""
        if self._games is None:
            self.refresh()
        assert self._games is not None
        return self._games

    @property
    def days(self) -> int:
        if not self.games:
            return 0
        return int(self.games[-1]['data']['day']) + 1

    def refresh(self) -> None:
        """Refresh the list of games."""
        _LOGGER.info("Getting bet data ...")
        games = chronicler.get_games(season=self.season, order='asc')
        incomplete = [g for g in games if not g['data']['gameComplete']]
        if incomplete:
            incomplete_day = incomplete[0]['data']['day']
            games = [g for g in games if g['data']['day'] < incomplete_day]
        self._games = games
        _LOGGER.info("OK. (%d games retrieved.)", len(self.games))


@dataclass
class BettingStats:
    games: int = 0
    bet: int = 0
    abstained: int = 0
    won: int = 0
    lost: int = 0
    profit: int = 0
    profit_per_bet: float = 0.0
    profit_per_game: float = 0.0
    profit_per_day: float = 0.0


class BetIntent(Enum):
    ABSTAIN = 0
    BET = 1


class BetSelection(Enum):
    HOME = 0
    AWAY = 1


class BetRisk(Enum):
    FAVORITE = 0
    UNDERDOG = 1
    NEUTRAL = 2    # Like. super theoretically ...


class BetResult(Enum):
    LOST = -1
    ABSTAINED = 0
    WON = 1


class Model:
    """
    Abstract betting-model.
    """

    def __init__(self, bet: int = 1000) -> None:
        self._bet = bet

    @property
    def bet(self) -> int:
        return self._bet

    @property
    def params(self) -> Dict[str, object]:
        return {'bet': self.bet}

    def __hash__(self) -> int:
        return hash(tuple((k, v) for k, v in self.params.items()))

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Model):
            return NotImplemented
        return (type(self), self.params) == (type(other), other.params)

    def __repr__(self) -> str:
        name = type(self).__name__
        params = (f"{name}={value}" for name, value in self.params.items())
        return "{:s}({:s})".format(name, ", ".join(params))

    def __str__(self) -> str:
        return f"<Model {self!r}>"

    @classmethod
    def betting_coefficient(cls, odds: float) -> float:
        """Return the betting coefficient for given odds"""
        if odds <= 0.5:
            return 2.0 + 0.0015 * ((100 * (0.5 - odds)) ** 2.2)
        return (3.206 / (1 + (0.443 * (odds - 0.5)) ** 0.95) - 1.206)

    @classmethod
    def betting_payout(cls, odds: float, bet: int) -> int:
        """Return the payout for winning a bet with the given odds."""
        return round(bet * cls.betting_coefficient(odds))

    @classmethod
    def home_data(cls, game: Game) -> Dict[str, Any]:
        return {k[4:]: v for k, v in game['data'].items() if k.startswith('home')}

    @classmethod
    def away_data(cls, game: Game) -> Dict[str, Any]:
        return {k[4:]: v for k, v in game['data'].items() if k.startswith('away')}

    # Favorability / Underdoggedness

    @classmethod
    def favored_odds(cls, game: Game) -> float:
        odds = (game['data']['homeOdds'], game['data']['awayOdds'])
        return float(max(odds))

    @classmethod
    def underdog_odds(cls, game: Game) -> float:
        odds = (game['data']['homeOdds'], game['data']['awayOdds'])
        return float(min(odds))

    @classmethod
    def home_favored(cls, game: Game) -> bool:
        return bool(game['data']['homeOdds'] > game['data']['awayOdds'])

    @classmethod
    def home_underdog(cls, game: Game) -> bool:
        return bool(game['data']['homeOdds'] < game['data']['awayOdds'])

    @classmethod
    def away_favored(cls, game: Game) -> bool:
        return bool(game['data']['awayOdds'] > game['data']['homeOdds'])

    @classmethod
    def away_underdog(cls, game: Game) -> bool:
        return bool(game['data']['awayOdds'] < game['data']['homeOdds'])

    # Who won?

    @classmethod
    def home_team_won(cls, game: Game) -> bool:
        return bool(game['data']['homeScore'] > game['data']['awayScore'])

    @classmethod
    def away_team_won(cls, game: Game) -> bool:
        return bool(game['data']['awayScore'] > game['data']['homeScore'])

    @classmethod
    def favored_team_won(cls, game: Game) -> bool:
        return (
            cls.home_favored(game) and (cls.home_team_won(game)) or
            cls.away_favored(game) and (cls.away_team_won(game))
        )

    @classmethod
    def underdog_team_won(cls, game: Game) -> bool:
        return (
            (cls.home_underdog(game) and cls.home_team_won(game)) or
            (cls.away_underdog(game) and cls.away_team_won(game))
        )

    # Model-dependent questions

    # Intent: Bet/Abstain

    def intent(self, game: Game) -> BetIntent:
        raise NotImplementedError

    def will_bet(self, game: Game) -> bool:
        return self.intent(game) == BetIntent.BET

    def will_abstain(self, game: Game) -> bool:
        return self.intent(game) == BetIntent.ABSTAIN

    # Selection: Home/Away (Meaningless when Intent is ABSTAIN.)

    def selected(self, game: Game) -> BetSelection:
        raise NotImplementedError

    def chose_home_team(self, game: Game) -> bool:
        return self.selected(game) == BetSelection.HOME

    def chose_away_team(self, game: Game) -> bool:
        return self.selected(game) == BetSelection.AWAY

    def selected_odds(self, game: Game) -> float:
        if self.selected(game) == BetSelection.AWAY:
            return float(game['data']['awayOdds'])

        assert self.selected(game) == BetSelection.HOME
        return float(game['data']['homeOdds'])

    # Risk: Did we select the favorite or the underdog?

    def risk(self, game: Game) -> BetRisk:
        odds = self.selected_odds(game)
        if odds > 0.50:
            return BetRisk.FAVORITE
        elif odds < 0.50:
            return BetRisk.UNDERDOG
        else:
            return BetRisk.NEUTRAL

    # Results: How'd we do?

    def result(self, game: Game) -> BetResult:
        if self.will_abstain(game):
            return BetResult.ABSTAINED
        elif self.chose_home_team(game) and self.home_team_won(game):
            return BetResult.WON
        elif self.chose_away_team(game) and self.away_team_won(game):
            return BetResult.WON
        else:
            return BetResult.LOST

    def profit(self, game: Game) -> int:
        result = self.result(game)
        profit = 0

        if result != BetResult.ABSTAINED:
            profit -= self.bet
            if result == BetResult.WON:
                profit += self.betting_payout(self.selected_odds(game), self.bet)

        return profit


class Homebet(Model):
    """Dumb model: Always choose the home team"""
    def intent(self, game: Game) -> BetIntent:
        return BetIntent.BET

    def selected(self, game: Game) -> BetSelection:
        return BetSelection.HOME


class Awaybet(Model):
    """Dumb mode: Always choose the away team"""
    def intent(self, game: Game) -> BetIntent:
        return BetIntent.BET

    def selected(self, game: Game) -> BetSelection:
        return BetSelection.AWAY


class Maxbet(Model):
    """Model in common usage: Bet the favorite at max amount if above a threshold."""
    def __init__(self, bet: int = 1000, threshold: float = 0.50) -> None:
        super().__init__(bet)
        self._threshold = threshold

    @property
    def threshold(self) -> float:
        return self._threshold

    @property
    def params(self) -> Dict[str, object]:
        return {
            'bet': self.bet,
            'threshold': self.threshold
        }

    def intent(self, game: Game) -> BetIntent:
        if self.favored_odds(game) >= self.threshold:
            return BetIntent.BET
        return BetIntent.ABSTAIN

    def selected(self, game: Game) -> BetSelection:
        # Favor the home-team for (very rare) even 50-50 splits.
        if game['data']['homeOdds'] >= game['data']['awayOdds']:
            return BetSelection.HOME
        else:
            return BetSelection.AWAY

    @classmethod
    def tune_threshold(cls, games: List[Game]) -> float:
        scores = []
        for i in range(0, 51):
            threshold = (50 + i) / 100
            model = cls(bet=1000, threshold=threshold)
            profit = sum(model.profit(game) for game in games)
            scores.append((profit, threshold))
        scores = sorted(scores, reverse=True)
        return scores[0][1]


class Homebias(Maxbet):
    def __init__(self, bet: int = 1000, threshold: float = 0.50,
                 bias: float = 0.00):
        super().__init__(bet, threshold)
        self._bias = bias

    @property
    def bias(self) -> float:
        return self._bias

    @property
    def params(self) -> Dict[str, object]:
        return {
            'bet': self.bet,
            'threshold': self.threshold,
            'bias': self.bias
        }

    def selected(self, game: Game) -> BetSelection:
        biased_home = game['data']['homeOdds'] + self.bias
        biased_away = game['data']['awayOdds'] - self.bias
        if biased_home >= biased_away:
            return BetSelection.HOME
        else:
            return BetSelection.AWAY

    def intent(self, game: Game) -> BetIntent:
        biased_home = game['data']['homeOdds'] + self.bias
        biased_away = game['data']['awayOdds'] - self.bias
        best = max(biased_home, biased_away)

        if best >= self.threshold:
            return BetIntent.BET
        return BetIntent.ABSTAIN


class Bets:
    def __init__(self, data: BetData):
        self.data = data
        self._payout_cache: Dict[Model, PayoutData] = {}

    @classmethod
    def make(cls, season: int) -> 'Bets':
        return cls(BetData(season))

    def refresh(self) -> None:
        self.data.refresh()

    @property
    def games(self) -> List[Game]:
        return self.data.games

    @property
    def days(self) -> int:
        return self.data.days

    def statistics(self, model: Model) -> BettingStats:
        stats = BettingStats()
        stats.games = len(self.games)

        for game in self.games:
            if model.intent(game) == BetIntent.ABSTAIN:
                stats.abstained += 1
            else:
                stats.bet += 1
            result = model.result(game)
            if result == BetResult.WON:
                stats.won += 1
            if result == BetResult.LOST:
                stats.lost += 1

        stats.profit = self.profit(model)
        stats.profit_per_bet = stats.profit / stats.bet if stats.bet else math.nan
        stats.profit_per_game = stats.profit / len(self.games) if self.games else math.nan
        stats.profit_per_day = stats.profit / self.days if self.days else math.nan

        return stats

    def print_statistics(self, model: Model) -> None:
        stats = self.statistics(model)
        print(f"total games: {stats.games:d}")
        print(f"bets made: {stats.bet:d}")
        print(f"bets abstained: {stats.abstained:d}")
        print(f"games won: {stats.won:d}")
        print(f"games lost: {stats.lost:d}")
        print(f"net profit: {stats.profit:d}")
        print(f"net profit per bet: {stats.profit_per_bet:5.2f}")
        print(f"net profit per game: {stats.profit_per_game:5.2f}")
        print(f"net profit per day: {stats.profit_per_day:5.2f}")

    def profit(self, model: Optional[Model] = None, efficiency: float = 1.00) -> int:
        """
        Calculate net profit given a particular betting cap and a betting strategy.

        Scale the resulting answer against the reported betting effiency!
        e.g. an efficiency of 14/24 (14 days per reality-day) is 0.58%.

        Efficiency is clamped to [0.00, 1.00] and rounded to two figures.
        """
        if model is None:
            model = Maxbet()

        if (model not in self._payout_cache or
            self._payout_cache[model][0] != len(self.games)):
            value = sum(model.profit(game) for game in self.games)
            self._payout_cache[model] = (len(self.games), value)

        efficiency = round(max(0.0, min(efficiency, 1.0)), 2)
        return round(efficiency * self._payout_cache[model][1])
